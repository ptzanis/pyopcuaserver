# PyOpcUaServer

**Simple Python code to generate OPC UA Server. :-)**

The current *quick* implementation is for an Arduino equipped with a BME280 sensor (Pressure, Temperature, Humidity) via I2C and analog ports monitoring via ADC for various input sensors.

Further use:
- Arduino 
- Raspberry Pi
- Linux
- Analog/Digital Sensors

## How to run:

`python PyOpcUa_Arduino_BME280_Analog.py`


## Some prerequisites :
- `virtualenv -p python3 envTools`
- `source envTools/bin/activate`
- `pip install serial`
- `pip install pyserial`
- `pip install opcua`

In case of `Permission denied: '/dev/ttyACM0'` run :
- ` sudo chmod a+rw /dev/ttyACM0 `

## Contact

Questions, comments, suggestions, or help?

**Polyneikis Tzanis**: <polyneikis.tzanis@cern.ch
