#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme; // I2C

unsigned long delayTime;

void setup() {
    Serial.begin(9600);

    unsigned status;
    status = bme.begin();   
    delayTime = 1000;

}


void loop() { 
    printValues();
    delay(delayTime);
}


void printValues() {
    Serial.println((String)bme.readTemperature()+" "+(String)(bme.readPressure()/100.0)+" "+(String)bme.readHumidity()+" "+analogRead(0)+" "+analogRead(1)+" "+analogRead(2)+" "+analogRead(3)+" "+analogRead(4)+" "+analogRead(5)/*+" "+analogRead(6)+" "+analogRead(7)+" "+analogRead(8)+" "+analogRead(9)+" "+analogRead(10)+" "+analogRead(11)+" "+analogRead(12)+" "+analogRead(13)+" "+analogRead(14)+" "+analogRead(15)*/);

}