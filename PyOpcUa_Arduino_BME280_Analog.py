import time
import socket
from serial.tools import list_ports
import serial
from opcua import Server

def findArduinoPort():

    ports = list(list_ports.comports())

    for portName, description, address in ports:
        if 'ttyACM0' in description:
            print('Found USB device on port {0:}'.format(portName))
            return portName
    return ''

def establishArduinoConnection():

    portNum = findArduinoPort()
    if portNum == '':
        print("\nNo Arduino found. Cannot continue...")
        return ''

    comPort = serial.Serial(portNum, 9600)
    return comPort

def initArduinoBMEItem(addspace, param, item):
    item = param.add_variable(addspace, item, 0)
    item.set_writable()
    return item

def initArduinoAnalog(addspace, param, x):
    item = param.add_variable(addspace, "A"+str(x), 0)
    item.set_writable()
    return item

def readAndPushBMEdata(comPort, tempNode, pressNode, humNode):

    read_serial = comPort.readline()
    val = read_serial.decode()
    val = val.strip()
    tempNode.set_value(val.split(' ')[0])
    pressNode.set_value(val.split(' ')[1])
    humNode.set_value(val.split(' ')[2])

def readAndPushAnalogData(comPort, analogNode, ai):

    read_serial = comPort.readline()
    val = read_serial.decode()
    val = val.strip()
    analogNode.set_value(val.split(' ')[3+ai])


def main():

    comPort = establishArduinoConnection()

    print("Waiting for OPC Server to initialize...")

    server = Server()
    url = "opc.tcp://"+str(socket.gethostname())+".cern.ch:48777"
    server.set_endpoint(url)
    name = "PyOpcUaServer"
    server.set_server_name(name)
    addspace = server.register_namespace(name)

    node = server.get_objects_node()

    Param = node.add_object(addspace, "BME280")
    tempNode = initArduinoBMEItem(addspace, Param, "Temperature")
    pressNode = initArduinoBMEItem(addspace, Param, "Pressure")
    humNode = initArduinoBMEItem(addspace, Param, "Humidity")

    Param = node.add_object(addspace, "Analog")

    analogItems = 6

    analogNode = []

    for i in range(analogItems):
        analogNode.append(initArduinoAnalog(addspace, Param, i))

    try:
        server.start()
    except ValueError:
        print("PyOpcUaServer is already running...")

    time.sleep(3)

    print(100*"*")
    print("*******         PyOpcUaServer started at {}".format(url)+"         *******")
    print(100*"*")

    while True:
        readAndPushBMEdata(comPort, tempNode, pressNode, humNode)
        for i in range(analogItems):
            readAndPushAnalogData(comPort, analogNode[i], i)


if __name__ == '__main__':
    main()
